﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace TestSerializer
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Subject> Subjects = new List<Subject>
            {
                new Subject { Count = 6776, Message = "Hello World!" },
                new Subject { Count = 6342, Message = "Hello World!" },
                new Subject { Count = 621, Message = "Hello World!" },
                new Subject { Count = 4, Message = "Hello World!" }
            };

            ISerializer<Subject> serializer = new MyXMLSerializer<Subject>();
            string contents = serializer.Serialize(Subjects.ToArray());
            Console.WriteLine(contents);

            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(contents));
            using (MyStreamreader<Subject> myStreamReader = new MyStreamreader<Subject>(stream, serializer))
            {
                ISorter<Subject> mysorter = new MySort<Subject>();
                IEnumerable<Subject> sorted = mysorter.Sort(myStreamReader, "Count");

                foreach (Subject s in sorted)
                {
                    Console.WriteLine("Count: {0} Message: {1}", s.Count, s.Message);
                }
            }

            Console.ReadKey();
        }
    }
}
