﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ExtendedXmlSerializer;

namespace TestSerializer
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSorterItems, String prop);
    }
    public class MySort<T> : ISorter<T>
    {
        public IEnumerable<T> Sort<T>(IEnumerable<T> notSorterItems, string prop)
        {
            return from u in notSorterItems
                   orderby u.GetType().GetProperty(prop).GetValue(u, null)
            select u;

        }
    }
}
