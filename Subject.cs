﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSerializer
{
    public class Subject
    {

        public string Message { get; set; }

        public int Count { get; set; }

    }
}
