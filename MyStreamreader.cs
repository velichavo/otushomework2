﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestSerializer
{
    public class MyStreamreader<T> : IEnumerable<T>, IDisposable
    {
        Stream stream;
        ISerializer<T> serializer;
        T[] items;
        int position = -1;

        private void Reset()
        {
            position = -1;
        }

        public MyStreamreader(Stream stream, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
        }
        public IEnumerator<T> GetEnumerator()
        {
            if (stream != null)
            {
                stream.Seek(0, SeekOrigin.Begin);
                items = serializer.Deserialize(stream);
                while (true)
                    if (position < items.Length - 1)
                    {
                        position++;
                        yield return items[position];
                    }
                    else
                    {
                        Reset();
                        yield break;
                    }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            if (stream != null)
                stream.Close();
            GC.SuppressFinalize(this);
        }
    }
}
