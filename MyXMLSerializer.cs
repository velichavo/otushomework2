﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace TestSerializer
{
    public interface ISerializer<T>
    {
        public string Serialize(T[] item);
        T[] Deserialize(Stream stream);
    }
    public class MyXMLSerializer<T> : ISerializer<T>
    {
        private readonly XmlWriterSettings XmlWriterSettings;

        public MyXMLSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
        }
        public T[] Deserialize(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
               .UseAutoFormatting()
               .UseOptimizedNamespaces()
               .EnableImplicitTyping(typeof(T[]), typeof(T))
               .Type<T[]>()
               .Name("List")
               .Create();
            return serializer.Deserialize<T[]>(new XmlReaderSettings { IgnoreWhitespace = false }, stream);
        }

        public string Serialize(T[] item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
            .UseAutoFormatting()
            .UseOptimizedNamespaces()
            .EnableImplicitTyping(typeof(T))
            .Create();

            return serializer.Serialize(XmlWriterSettings, item);
        }
    }

}
